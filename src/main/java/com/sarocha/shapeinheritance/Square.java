/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.shapeinheritance;

/**
 *
 * @author Sarocha
 */
public class Square extends Rectangle {
    private double side = width;

    public Square (double side) {
        super(side,side);
        System.out.println("Square created");
    }
    
    @Override
    public void print() {
        System.out.println("Square : side : "+side+" area = "+calArea());
    }
}
