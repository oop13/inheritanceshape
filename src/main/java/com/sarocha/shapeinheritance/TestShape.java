/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.shapeinheritance;

/**
 *
 * @author Sarocha
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle1 = new Circle (3);
        circle1.calArea();
        circle1.print();
        space();
        Triangle triangle1 = new Triangle (3,9);
        triangle1.calArea();
        triangle1.print();
        space();
        Rectangle rectangle1 = new Rectangle (4,5);
        rectangle1.calArea();
        rectangle1.print();
        space();
        Square square1 = new Square (5);
        square1.calArea();
        square1.print();
        space();
        System.out.println("Circle is Shape : " + (circle1 instanceof Shape));
        System.out.println("Rectangle is Shape : " + (rectangle1 instanceof Shape));
        System.out.println("Square is Shape : " + (square1 instanceof Shape));
        System.out.println("Square is Rectangle : " + (square1 instanceof Rectangle));
        System.out.println("Triangle is Shape : " + (triangle1 instanceof Shape));
        space();
        space();
        Shape[] shapes = {circle1,rectangle1,square1,triangle1};
        for (int i=0;i<shapes.length;i++) {
            shapes[i].calArea();
            shapes[i].print();
        }
    }
    public static void space() {
        System.out.println();
    }

}
