/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.shapeinheritance;

/**
 *
 * @author Sarocha
 */
public class Triangle extends Shape {
    private double tri = 0.5;
    private double base;
    private double height;
    
    public Triangle (double base,double height) {
        this.base = base;
        this.height = height;
        System.out.println("Triangle created");
    }
    
    @Override
    public double calArea() {
        return tri*base*height;
    }
    
    @Override
    public void print() {
        System.out.println("Triangle : base : "+base+", height : "+height+" area = "+calArea());
    }  
}
