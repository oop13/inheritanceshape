/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.shapeinheritance;

/**
 *
 * @author Sarocha
 */
public class Rectangle extends Shape {
    protected double width;
    protected double length;
    
    public Rectangle (double width,double length) {
        this.width = width;
        this.length = length;
        System.out.println("Rectangle created");
    }
    @Override
    public double calArea() {
        return width*length;
    }
   
    @Override
    public void print() {
        System.out.println("Rectangle : width : "+width+", length : "+length+" area = "+calArea());
    }  
}
