/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.shapeinheritance;

/**
 *
 * @author Sarocha
 */
public class Circle extends Shape {
    private double r;
    private double pi = 22.0/7;
    
    public Circle (double r) {
        this.r = r;
        System.out.println("Circle created");
    }
    
    @Override
    public double calArea() {
        return pi*r*r;
    }
    
    @Override
    public void print() {
        System.out.println("Circle : r : "+r+", pi : "+pi+" area = "+calArea());
    }   
}
